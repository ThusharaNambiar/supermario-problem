class SuperMario:
    
    #defining constructor
    
    def __init__(self,rows,columns):
        
        self.m=rows
        self.n=columns
        
    #defining class methods
    
    def input_matrix(self):
        
        self.matrix = []; columns = []
        # initialize the number of rows
        for i in range(0,self.m):
            self.matrix += [0]
        # initialize the matrix
        for i in range (0,self.m):
            self.matrix[i] = [0]*self.n
        for i in range (0,self.m):
            for j in range (0,self.n):
                self.matrix[self.m-1-i][j] = raw_input()
        return 
        
   #define function 'action' to decide whether forward or jump&forward 
    def action(self):
        
        j=0
        navigation_path=[]
        for i in range(self.n-1):
            if(self.matrix[j][i+1]=='e'):
                navigation_path.append('f')
            else:
                navigation_path.append('(j,f)')
                j+=1
            while (self.matrix[j-1][i]=='e' and j>0):
                j-=1
        return navigation_path
        

    
        
#create an object of the class


rows = int(input('number of rows, m = '))
columns = int(input('number of columns, n = '))

Obj_Mario=SuperMario(rows,columns)

Obj_Mario.input_matrix()
nav_path=Obj_Mario.action()
print "  Navigation Path   ", nav_path    
